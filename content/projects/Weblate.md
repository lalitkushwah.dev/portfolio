---
date: '2020-01-10'
title: 'Localize/Internationalize frontend project with Weblate'
github: 'https://github.com/LalitKushwah/react-weblate'
external: 'https://lalit-kushwah.medium.com/localize-internationalize-frontend-project-with-weblate-b367768e730e'
tech:
  - Weblate
  - Docker
  - React
company: 'Delivery Hero SE'
showInProjects: true
---

An aided translation tool, Weblate saves both developers and translators time. Make your users happier! Browse the documentation to learn more.
