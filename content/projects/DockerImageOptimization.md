---
date: '2020-03-27'
title: 'Optimize react boilerplate project’s docker file to reduce the image’s size from GBs to MBs'
github: 'https://github.com/LalitKushwah/docker-optimization'
external: 'https://lalit-kushwah.medium.com/optimize-react-boilerplate-projects-docker-to-reduce-the-image-s-size-from-gbs-to-mbs-b2eb36bd249e'
tech:
  - Docker
  - React
company: 'Delivery Hero SE'
showInProjects: true
---

In this blog, I have shared the common docker instructions and how it leads to making your docker image’s size bigger and how can you correct this which leads to significant improvement in image size
