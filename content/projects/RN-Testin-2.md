---
date: '2018-12-01'
title: 'Unit Testing in RN - Part 2'
github: ''
external: 'How to write Unit test cases for the Modal component? [Part 3/3]'
tech:
  - React Native
  - Testing Library

company: ''
showInProjects: true
---

An extension blog of unit testing in React native series
