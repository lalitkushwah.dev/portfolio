---
date: '2018-12-29'
title: 'Write-Concern in MongoDB'
github: ''
external: 'https://lalit-kushwah.medium.com/do-you-know-about-write-concern-in-mongodb-9b4688c195f6'
tech:
  - MongoDB
showInProjects: true
---

Write concern describes the level of acknowledgment requested from MongoDB for write operations to a standalone mongod or to replica sets or to sharded clusters. In sharded clusters, mongos instances will pass the write concern on to the shards.
