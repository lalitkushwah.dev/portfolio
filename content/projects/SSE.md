---
date: '2019-11-12'
title: 'Harness the power of Server-Sent Events (SSE)'
github: 'https://github.com/LalitKushwah/sse'
external: 'https://medium.com/stories-from-upstatement/building-a-headless-mobile-app-cms-from-scratch-bab2d17744d9'
tech:
  - Node
  - React
company: ''
showInProjects: true
---

SSE (Server-Sent Events) is one of those powerful APIs that provides the uni-directional communication feature (unlike WebSocket which is bi-directional).
