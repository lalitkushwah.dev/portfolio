---
date: '2017-11-01'
title: 'Split in JS'
github: ''
external: 'https://lalit-kushwah.medium.com/stop-using-split-to-split-string-with-space-in-javascript-7e1a1409cb0b'
tech:
  - Javascript
company: ''
showInProjects: true
---

There are some precautions or edge cases which developer should know about while working with <b>split</b> function
