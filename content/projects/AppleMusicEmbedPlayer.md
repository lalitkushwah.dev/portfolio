---
date: '2017-12-01'
title: 'Unit Testing in React Native'
github: ''
external: 'https://lalit-kushwah.medium.com/how-to-write-unit-test-cases-using-react-native-testing-library-part-2-2-59ebe488fcee'
tech:
  - React Native
  - Testing Library
company: 'Walmart'
showInProjects: true
---

When you start writing unit test cases, there are plenty of things that could improve your test cases' quality
