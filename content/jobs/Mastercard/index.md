---
date: '2017-12-21'
title: 'Software Engineer II'
company: 'Mastercard'
location: 'Pune, India'
range: 'June, 2018 - March 2021'
url: 'https://www.mastercard.com/'
---

- Developed a technology-agnostic framework capable of creating 400+ web pages website in just 2-3 weeks.
- Developed Custom CLI to support the framework integration in consumer apps
- Mentored Interns who eventually joined the org as FTE
