---
date: '2018-04-01'
title: 'Software Engineer III'
company: 'Walmart Global Tech India'
location: 'Banglore, India'
range: 'March 2021 - May 2022'
url: 'https://www.walmart.com/'
---

- Developed enterprise app using React Native called <b>Next</b>. It is saving ~1.5M$/Year for Walmart.
- Developed Web application to automate store re- modeling along with mentoring interns to integrate Adobe Analytics with valuable metrics
