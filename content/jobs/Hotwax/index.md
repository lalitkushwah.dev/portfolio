---
date: '2017-04-01'
title: 'Enterprise Software Engineer'
company: 'HotWax Systems'
location: 'Indore, India'
range: 'April, 2017 - March, 2018'
url: 'https://www.hotwaxsystems.com/'
---

- Developed B2B mobile apps to manage Inventory, Warehouse, Shipping, Billing, Stock Move
- Developed APOS (POS on android tablet)
