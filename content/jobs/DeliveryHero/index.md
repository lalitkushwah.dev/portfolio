---
date: '2018-05-14'
title: 'Software Engineer'
company: 'Delivery Hero SE'
location: 'Berlin, Germany'
range: 'June 2022 - Present'
url: 'https://www.deliveryhero.com/'
---

- Developed B2B ordering web application to enable the restaurant, logistics vendor and platforms for smooth functioning
- Building <b>Product Information Management</b> application that is responsible for mantaining around 50M products information and streaming information to all platforms (Foodora, FoodPanda, Pandora, yemeksepeti, LATAM, E-Food)
